import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable()
export class SharedService {
    selectedId: string;
    profile: IUserProfile;
    profiles: IUserProfile[];

    constructor(
        public db: AngularFirestore,
        public router: Router
    ) {
        this.profiles = [];
        this.profile = {
            id: '',
            name: '---',
            profile_picture: 'https://firebasestorage.googleapis.com/v0/b/khonochat.appspot.com/o/profile_pictures%2Fprofile_pic_default.png?alt=media&token=ab81322d-297e-4cce-985c-3f2d087aa565',
            status: 1
        };
        this.init();
    }

    setProfile(profile: IUserProfile) {
        this.profile = profile;
        localStorage.setItem('selected_profile', JSON.stringify(profile));
    }

    async init() {
        const profiles = await this.db.collection('chat_participants').ref.get();
        this.profiles = profiles.docs.map(item =>  item.data() as IUserProfile);
        const profile = JSON.parse(localStorage.getItem('selected_profile'));
        if (profile === null) {
           this.profile = this.profiles[0];
        } else {
            this.profile = profile;
        }
      }

    getStatusColor(id: number) {
        let color = 'notset';
        switch (id) {
          case 1: color = 'available';
            break;
          case 2: color = 'busy';
            break;
          case 3: color = 'unavailable';
            break;
          default:
            break;
        }
        return color;
      }

      
}
