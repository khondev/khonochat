import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { JsonpInterceptor } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  statusColor: string;
  toId: string;
  message: string;
  inMessages: IMessages[];
  outMessages: IMessages[];
  messages: IMessages[];
  constructor(
    public shared: SharedService,
    public db: AngularFirestore
  ) {
    this.statusColor = this.shared.getStatusColor(this.shared.profile.status);
    this.outMessages = [];
    this.inMessages = [];
    this.message = '';
    this.initMessages();
  }

  ngOnInit() {
  }

  getSelectedIndex() {
    let index = 0;
    switch (this.shared.profile.status) {
      case 1: index = 0;
        break;
      case 2: index = 1;
        break;
      case 3: index = 2;
        break;
    }
    return index;
  }

  async initMessages() {
    this.db.collection('messages').ref
    .where('from', '==', this.shared.profile.id)
    .onSnapshot(q => {
      this.outMessages = q.docs.map(item => item.data() as IMessages);
      this.messages = [];
      for (const out of this.outMessages) {
        this.messages.push(out);
      }
      for (const inMesage of this.inMessages) {
        this.messages.push(inMesage);
      }
      this.messages = this.messages.sort((a, b) => {
        if (a.time < b.time) {
          return 1;
        } else if (a.time > b.time) {
          return -1;
        } else {
          return 0;
        }
      });
    });
    this.db.collection('messages').ref
    .where('to', '==', this.shared.profile.id)
    .onSnapshot(q => {
      this.inMessages = q.docs.map(item => item.data() as IMessages);
      this.messages = [];
      for (const out of this.outMessages) {
        this.messages.push(out);
      }
      for (const inMesage of this.inMessages) {
        this.messages.push(inMesage);
      }
      this.messages = this.messages.sort(( a, b) => {
        if (a.time < b.time) {
          return -1;
        } else if (a.time > b.time) {
          return 1;
        } else {
          return 0;
        }
      });
    });
  }

  selectTo($event) {
    this.toId = $event.target.value;
  }

  getUser(id) {
    const index = this.shared.profiles.findIndex(q => q.id === id);
    if (index > -1) {
      return this.shared.profiles[index].name;
    }
  }

  async sendMessage() {
    const msg: IMessages = {
      from: this.shared.profile.id,
      message: this.message,
      time: new Date(),
      to: this.toId
    };
    await this.db.collection('messages').add(msg);
    this.message = '';
  }

  async updateStatus($event) {
    const update = await this.db.collection('chat_participants')
      .doc(this.shared.profile.id)
      .update({status: parseInt($event.target.value, null)});
     this.shared.profile.status = parseInt($event.target.value, null);
     localStorage.setItem('selected_profile', JSON.stringify(this.shared.profile));
     this.statusColor = this.shared.getStatusColor(this.shared.profile.status);
  }
}
