interface IUserProfile {
    id: string;
    name: string;
    profile_picture: string;
    status: number;
}
